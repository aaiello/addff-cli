#!/usr/bin/env node

const chalk = require("chalk");
const clear = require("clear");
const figlet = require("figlet");

const inquirer = require("./lib/inquirer");

clear();

console.log(
  chalk.yellow(figlet.textSync("Feature Flags", { horizontalLayout: "full" }))
);

const getEnvName = ffName => {
  return (
    "WEBAPP_" +
    ffName
      .split(/(?=[A-Z])/)
      .join("_")
      .toUpperCase()
  );
};

const updateFile = (fileName, patterns) => {
  var fs = require("fs");
  fs.readFile(fileName, "utf8", function(err, data) {
    if (err) {
      return console.log(err);
    }

    let result = data;
    patterns.forEach(element => {
      result = result.replace(
        element.pattern,
        element.pattern + "\n" + element.newFlag
      );
    });

    fs.writeFile(fileName, result, "utf8", function(err) {
      if (err) return console.log(err);
    });
  });
};

const run = async () => {
  try {
    const names = await inquirer.askFFName();

    const name = names.ffname;
    const envName = getEnvName(name);

    updateFile("charts/pax-web-internal/templates/pax-web-internal.yaml", [
      {
        pattern:
          "WEBAPP_ENABLE_BTC: {{ .Values.enableBtc | default false | quote }}",
        newFlag: `  ${envName}: {{ .Values.${name} | default false | quote }}`
      }
    ]);
    updateFile("charts/pax-web/templates/pax-web.yaml", [
      {
        pattern:
          "WEBAPP_ENABLE_BTC: {{ .Values.enableBtc | default false | quote }}",
        newFlag: `  ${envName}: {{ .Values.${name} | default false | quote }}`
      }
    ]);
    updateFile("webapp/public/config.__CACHENONCE__.js", [
      { pattern: 'enableBtc: "false",', newFlag: `  ${name}: "false",` }
    ]);
    updateFile("webapp/public/config.js.envtpl", [
      {
        pattern: 'enableBtc: "${WEBAPP_ENABLE_BTC}",',
        newFlag: `  ${name}: "${envName}",`
      }
    ]);
    updateFile("webapp/src/config.ts", [
      { pattern: 'enableBtc: "",', newFlag: `  ${name}: "",` },
      {
        pattern: "enableBtc: process.env.WEBAPP_ENABLE_BTC,",
        newFlag: `  ${name}: process.env.${envName},`
      }
    ]);
    updateFile("webapp/src/utils/configHelpers.ts", [
      {
        pattern:
          'export const isBtcEnabled = () => config.enableBtc === "true";',
        newFlag: `export const is${name.replace(
          "enable",
          ""
        )}Enabled = () => config.${name} === "true";`
      }
    ]);

    console.log(chalk.green("All done!"));
  } catch (err) {
    if (err) {
      console.log(err);
    }
  }
};

run();
