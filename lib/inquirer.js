const inquirer = require("inquirer");

module.exports = {
  askFFName: () => {
    const questions = [
      {
        name: "ffname",
        type: "input",
        message: "Enter FF friendly name (ex. enableBtc):",
        validate: function(value) {
          if (value.length) {
            return true;
          } else {
            return "Please enter FF friendly name.";
          }
        }
      }
    ];
    return inquirer.prompt(questions);
  }
};
